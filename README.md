# My Contacts

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project.

### Installing

What things you need to install the software and how to install them.

* ###### Installing NodeJs

First, install [NodeJs](https://nodejs.org/en/). Then, install the latest Cordova and Ionic command-line tools in your terminal. Follow the [Android](https://cordova.apache.org/docs/en/7.x/guide/platforms/android/) and [iOS](https://cordova.apache.org/docs/en/7.x/guide/platforms/ios/) platform guides to install required tools for development.

* ###### Installing Cordova ionic

```
npm install -g cordova ionic
```

* After that you need to [Download](https://bitbucket.org/pradipkachhadiya/mycontacts.git) or Clone repository from [here](https://bitbucket.org/pradipkachhadiya/mycontacts.git), Move inside cloned directory and install packages by typing following command into terminal.

```
npm install
```

Your setup is completed, Now you are on the way to run application.

## Running the App

To run the app you just need to type folloing command into terminal.

```
ionic serve
```

## Built With

* [Ionic - 3.19.1](https://ionicframework.com/)

## Authors

* **Pradip Kachhadiya**
[https://nodejs.org/en/]: https://nodejs.org/en/ "NodeJs"

Email(pradip.sutex@gmail.com) me for any query.