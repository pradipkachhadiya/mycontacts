import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController, ItemSliding } from 'ionic-angular';

import { Contact } from '../../models/item';
import { Contacts, Api, UtilServiceProvider } from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html'
})
export class ListMasterPage {
  contactList: any = [];

  constructor(public navCtrl: NavController,
    private toastCtrl: ToastController,
    public contacts: Contacts,
    public api: Api,
    public _utilService: UtilServiceProvider,
    public modalCtrl: ModalController) {
    this.getContactList();
  }

  public getContactList() {
    this._utilService.showWaitingLoading();
    this.api.get('users').subscribe((res: any) => {
      this._utilService.dismissWaitingLoading();
      for (let index = 0; index < res.length; index++) {
        if (res[index].picture)
          res[index].picture = this.api.pictureBaseUrl + res[index].picture;
      }
      this.contactList = res;
      localStorage.setItem("contactList",JSON.stringify(this.contactList))
    }, (err: any) => {
      this._utilService.dismissWaitingLoading();
      this._utilService.showErrorCall(err);
    })
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
  }

  /**
   * Prompt the user to add a new item. This shows our CreateContactPage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addContact(contact: any = {}) {
    let addModal = this.modalCtrl.create('CreateContactPage', { contact: contact });
    addModal.onDidDismiss(contact => {
      this.getContactList();
      // if (contact) {
      //     this.contacts.add(contact);
      // }
    })
    addModal.present();
  }

  /**
   * Delete an item from the list of items.
   */
  deleteContact(contact,index:number) {
    this._utilService.showWaitingLoading()
    this.api.delete('users/'+contact.id).subscribe(res=>{      
      this._utilService.dismissWaitingLoading()
      this.contactList.splice(index,1);
    },err=>{
      this._utilService.dismissWaitingLoading()
      this._utilService.showErrorCall(err);      
    });
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(contact: Contact) {
    this.navCtrl.push('ItemDetailPage', {
      item: contact
    });
  }

  /**
   * Call to specific contact
   */
  call(contact: any, slidingItem: ItemSliding) {
    let toast = this.toastCtrl.create({
      message: 'Your call is forwarding to ' + contact.primary_contact_number + ' of ' + contact.name,
      duration: 3000,
      position: 'bottom'
    });
    slidingItem.close()
    toast.present()
  }


}
