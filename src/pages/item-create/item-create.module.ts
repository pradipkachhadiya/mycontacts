import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { CreateContactPage } from './item-create';

@NgModule({
  declarations: [
    CreateContactPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateContactPage),
    TranslateModule.forChild()
  ],
  exports: [
    CreateContactPage
  ]
})
export class ContactCreatePageModule { }
