import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Api } from '../../providers/api/api';
import { UtilServiceProvider } from '../../providers/util-service/util-service';
@IonicPage()
@Component({
  selector: 'page-item-create',
  templateUrl: 'item-create.html'
})
export class CreateContactPage {
  @ViewChild('fileInput') fileInput;

  isReadyToSave: boolean;

  item: any;

  form: FormGroup;

  isEditable: boolean;

  constructor(public navCtrl: NavController,
    public params: NavParams,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    public api: Api,
    public _utilService: UtilServiceProvider,
    public camera: Camera) {
    // console.log('UserId', );
    if (params.get('contact') && params.get('contact').id) {
      this.isEditable = true;
      this.form = formBuilder.group({
        picture: [params.get('contact').picture],
        name: [params.get('contact').name, Validators.required],
        about: [params.get('contact').about],
        primary_contact_number: [params.get('contact').primary_contact_number, Validators.required],
        work_contact_number: [params.get('contact').work_contact_number],
        email: [params.get('contact').email],
        website: [params.get('contact').website],
        id: [params.get('contact').id]
      });
      this.isReadyToSave = true;
    } else {
      this.isEditable = false;
      this.form = formBuilder.group({
        picture: [''],
        name: ['', Validators.required],
        about: [''],
        primary_contact_number: ['', [Validators.required]],
        work_contact_number: [''],
        email: [''],
        website: ['']
      });
    }

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ionViewDidLoad() {
  }

  getPicture() {
    if (Camera['installed']()) {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 96,
        targetHeight: 96
      }).then((data) => {
        this.form.patchValue({ 'picture': 'data:image/jpg;base64,' + data });
      }, (err) => {
        alert('Unable to take photo');
      })
    } else {
      this.fileInput.nativeElement.click();
    }
  }

  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {

      let imageData = (readerEvent.target as any).result;
      this.form.patchValue({ 'picture': imageData });

      if(this.form.value.id){
        this.api.put("users/picture",{
          "id":this.form.value.id,
          "picture":imageData.split(",")[1]
        }).subscribe(res=>{
          this._utilService.dismissWaitingLoading();
          this._utilService.showToast("Picture changed");
        },err=>{
          this._utilService.dismissWaitingLoading();
          this._utilService.showErrorCall(err);
        })
      }
    };

    reader.readAsDataURL(event.target.files[0]);
  }

  getProfileImageStyle() {
    return 'url(' + this.form.controls['picture'].value + ')'
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  done() {
    if (!this.form.valid) { return; }

    this._utilService.showWaitingLoading();

    // if(this._utilService.ValidURL(this.form.value.picture))
    //   delete this.form.value.picture;
    if (this.form.value.picture)
      this.form.value.picture = this.form.value.picture.split(",")[1]

    if(this.form.value.id){
      delete this.form.value.picture;
      this.api.put("users", this.form.value).subscribe((res: any) => {
        this._utilService.dismissWaitingLoading();
        this.viewCtrl.dismiss(this.form.value);
      }, err => {
        this._utilService.dismissWaitingLoading();
        this._utilService.showErrorCall(err);
      })
    } else{
      this.api.post("users", this.form.value).subscribe((res: any) => {
        this._utilService.dismissWaitingLoading();
        this.viewCtrl.dismiss(this.form.value);
      }, err => {
        this._utilService.dismissWaitingLoading();
        this._utilService.showErrorCall(err);
      })
    } 

  }
}
