import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Events, LoadingController, ToastController, AlertController } from 'ionic-angular';

@Injectable()
export class UtilServiceProvider {

  public loading: any;
  public toast: any;
  public picObj: any = {};
  public userProfile: any;

  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public events: Events,
  ) {

  }

  public ValidURL(str:any) {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return pattern.test(str);
  }
  

  public showWaitingLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'Please Wait...'
      });
      this.loading.present();
    }
  }

  public dismissWaitingLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  presentAlert(title: string, message: any, buttonText: string[] = ['Ok']) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: buttonText
    });
    alert.present();
  }

  public showToast(msg: string = "Something went wrong. Please try again later...",
    showCloseButton: boolean = false,
    position: string = "bottom",
    duration: number = 1500,
  ) {
    showCloseButton = false;
    var config = {
      message: msg,
      duration: duration,
      position: position,
      showCloseButton: showCloseButton
    };

    // if (showCloseButton)
    //   delete config.duration;

    this.toast = this.toastCtrl.create(config);
    this.toast.present()
  }

  public showErrorToast(msg: string = "Something went wrong. Please try again later...",
    showCloseButton: boolean = false,
    title: string = "Oops!",
  ) {
    // return this.toastrService.error(msg, title, options);
    // this.presentAlert(title, msg)
    this.showToast(title + " " + msg, showCloseButton)
  }

  public dismissToast() {
    if (this.toast)
      this.toast.dismiss();
  }

  public showErrorCall(err: any, show: number = 0) {
    if (err._body) {
      try {
        let errStr = '';
        let errBody: any = JSON.parse(err._body);
        Object.keys(errBody).map(val => errStr += errBody[val]);
        if (err.status === 404) {
          if (show == 0)
            this.showErrorToast(errStr, true);
        } else if (err.status == 422 && errBody.message) {
          if (show == 0)
            this.showErrorToast(errBody.message);
        } else {
          if (show == 0)
            this.showErrorToast(errStr);
        }
        if (err.status === 401 && localStorage.getItem('isLoggedin')) {
          this.events.publish('call:logout')
        }
      } catch (error) {
        this.showErrorToast();
      }
    } else if (err.message) {
      this.showErrorToast(err.message);
    } else {
      this.showErrorToast();
    }
  }

  public showConfirm(title: string, message: string) {
    return new Promise((resolve, reject) => {
      let confirm = this.alertCtrl.create({
        title: title,
        message: message,
        buttons: [
          {
            text: 'No',
            handler: () => {
              reject(false);
            }
          },
          {
            text: 'Yes',
            handler: () => {
              resolve(true);
            }
          }
        ]
      });
      confirm.present();
    });
  }  
}
