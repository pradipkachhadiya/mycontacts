import { Injectable } from '@angular/core';

import { Contact } from '../../models/item';

@Injectable()
export class Contacts {
  items: Contact[] = [];

  defaultItem: any = {
    "name": "Burt Bear",
    "profilePic": "assets/img/speakers/bear.jpg",
    "about": "Burt is a Bear.",
  };


  constructor() {
    let items = JSON.parse(localStorage.getItem('contactList'));

    if(items){
      for (let item of items) {
        this.items.push(new Contact(item));
      }
    }else{
      items=[];
    }
    
  }

  query(params?: any) {
    if (!params) {
      return this.items;
    }
  
    return this.items.filter((item) => {
      for (let key in params) {        
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  add(item: any) {
    if(item.id){
      for (let index = 0; index < this.items.length; index++) {
        if(this.items[index]['id'] == item.id){
          this.items[index] = item;
        }
      }
    }else{
      item['id']= new Date().getTime()
      this.items.push(item);
    }
   
    // this.setToLocalStorage();
  }

  delete(item: Contact) {
    this.items.splice(this.items.indexOf(item), 1);
    // this.setToLocalStorage();
  }

  public setToLocalStorage(){
    localStorage.setItem('contactList',JSON.stringify(this.items))
  }
}
